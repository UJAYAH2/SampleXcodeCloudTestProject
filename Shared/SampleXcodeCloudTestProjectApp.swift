//
//  SampleXcodeCloudTestProjectApp.swift
//  Shared
//
//  Created by Nadeeshan Jayawardana on 2021-09-23.
//

import SwiftUI

@main
struct SampleXcodeCloudTestProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
